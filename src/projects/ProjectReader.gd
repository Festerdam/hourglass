class_name ProjectReader
extends Node
# Reader for project.godot files. Needed due to differences in that file's
# contents between Godot 3 and 4.


var path: String setget _set_path
# Error describing whether opening and reading the file succeeded.
var error: int setget _set_error

var _project_godot3: ConfigFile
var _contents: String
var _major_version: int
var _re: RegEx


func _init(path: String):
	_project_godot3 = ConfigFile.new()
	_re = RegEx.new()
	project_load(path)


func project_load(project_godot_path: String) -> void:
	var file := File.new()
	path = project_godot_path
	error = file.open(path, File.READ)
	if error != OK:
		return
	_contents = file.get_as_text()
	
	_re.compile("(?<=config_version=)\\d*")
	var result: RegExMatch = _re.search(_contents)
	if result:
		_major_version = 4 if int(result.get_string()) >= 5 else 3
	else:
		error = ERR_PARSE_ERROR
		return
	
	if _major_version == 3:
		error = _project_godot3.parse(_contents)
	file.close()


func get_project_name() -> String:
	var untitled_project: String = "Untitled Project"
	assert(error == OK)
	if _major_version == 4:
		_re.compile("(?<=config\\/name=\\\").*(?=\\\")")
		var result: RegExMatch = _re.search(_contents)
		if result:
			return result.get_string()
		else:
			return untitled_project
	else:
		return _project_godot3.get_value("application", "config/name", untitled_project)


func get_icon_path() -> String:
	assert(error == OK)
	if _major_version == 4:
		_re.compile("(?<=config\\/icon=\\\")res:\\/\\/.+(?=\\\")")
		var result: RegExMatch = _re.search(_contents)
		return result.get_string() if result else null
	else:
		return _project_godot3.get_value("application", "config/icon")


func get_main_scene_path() -> String:
	assert(error == OK)
	if _major_version == 4:
		_re.compile("(?<=run\\/main_scene=\\\")res:\\/\\/.+(?=\\\")")
		var result: RegExMatch = _re.search(_contents)
		return result.get_string() if result else null
	else:
		return _project_godot3.get_value("application", "run/main_scene")


# Do not use
func _set_error(new_error: int) -> void:
	assert(false, "Attempted setting a read-only property.")


# Do not use
func _set_path(new_path: String) -> void:
	assert(false, "Attempted setting a read-only property.")
