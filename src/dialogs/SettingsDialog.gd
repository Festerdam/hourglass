extends BaseDialog

onready var file_dialog := $FileDialog
onready var versions_path_setting := $GridContainer/HBoxContainer/VersionsPathSetting
onready var versions_path_reset := $GridContainer/Control/VersionsPathReset
onready var keep_open_setting := $GridContainer/KeepOpenSetting
onready var keep_open_reset := $GridContainer/Control2/KeepOpenReset

func _ready() -> void:
	self.content_size = Vector2(600, 300)
	self.title = tr("Settings")
	self.ok_text = tr("Save")


func _on_SettingsDialog_confirmed():
	Config.set_versions_path(versions_path_setting.text, false)
	Config.set_keep_open(keep_open_setting.pressed, false)
	Config.save()


func _on_SelectFolder_pressed():
	file_dialog.popup()


func _on_FileDialog_dir_selected(dir):
	versions_path_setting.text = dir
	_on_VersionsPathSetting_text_changed(dir)


func _on_VersionsPathSetting_text_changed(new_text):
	if new_text == Config.get_versions_path():
		versions_path_reset.hide()
	else:
		versions_path_reset.show()


func _on_VersionsPathReset_pressed():
	versions_path_setting.text = Config.get_versions_path()
	_on_VersionsPathSetting_text_changed(versions_path_setting.text)


func _on_KeepOpenSetting_toggled(button_pressed):
	if button_pressed == Config.get_keep_open():
		keep_open_reset.hide()
	else:
		keep_open_reset.show()


func _on_KeepOpenReset_pressed():
	keep_open_setting.pressed = Config.get_keep_open()


func _on_SettingsDialog_about_to_show():
	versions_path_setting.text = Config.get_versions_path()
	_on_VersionsPathSetting_text_changed(versions_path_setting.text)
	keep_open_setting.pressed = Config.get_keep_open()
	_on_KeepOpenSetting_toggled(keep_open_setting.pressed)
